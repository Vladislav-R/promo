import {Given,When, Then, And} from "cypress-cucumber-preprocessor/steps";




Given(`you open the site`, () => {
    cy.visit('https://5element.by/')
    cy.log("Step 1")
});
Then('go to the kitchen section',() => {
    cy.contains('Техника для кухни').click()
    cy.log("steps 2")
});
Then('go to refrigerators', () => {
    cy.get( '#filters-0 > .filter-body > :nth-child(1) > :nth-child(1) > a').should ('be.visible') . click()
    cy.log("steps 3")
});
Then('choose with bottom freezer' , () => {
    cy.get(':nth-child(2) > .item-box > .item-box-link'). should ('be.visible'). click()
    cy.log("steps 4 ")
});
Then('choose LG' , () => {
    cy.get( '#filter-684313 > .filter-body > :nth-child(2) > .inp-box > .inp-box__label > .inp-box__view').should('#filter-684313 > .filter-body > :nth-child(2) > .inp-box > .inp-box__label > .inp-box__view') . click()
    cy.get('#filter-716065 > .filter-body > :nth-child(1) > .inp-box > .inp-box__label > .inp-box__view').click()
    cy.get('#filter-684295 > .filter-body > :nth-child(1) > .inp-box > .inp-box__label > .inp-box__view').click();
    cy.log("steps 5")
});
Then('we order and arrange', () => {
    cy.get(':nth-child(1) > :nth-child(4) > .card-product-full > :nth-child(3) > .c-controls > .btn--index > .ic-tap').should('be.visible'). click()
     cy.get('#one-click-name').type('Ivan')
    cy.get('#one-click-phone').type('291111111')
    cy.get('.modal-popup-form > :nth-child(5) > .btn'). should( 'be.visible') .click()
    cy.log("steps 6")
});
