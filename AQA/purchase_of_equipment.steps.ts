import {Given,When, Then, And} from "cypress-cucumber-preprocessor/steps";

Given('site opens 5element' , () => {
    cy.visit('https://5element.by/')
    cy.log("Step 1")
});

And ('select the first section with technology',() => {
    cy.get('[aria-label="3 / 13"] > .nav-item').
    should ('[aria-label="3 / 13"] > .nav-item'). click()
    cy.log("Step 2")
});

And('choose the first technique', () => {
    cy.contains('swiper-slide').
    should('swiper-slide').click()
    cy.log("Step 3")
});

And('add it to the cart , check that the product has been added' , () => {
    cy.get('.pp-controls > .btn--block').
    should('.pp-controls > .btn--block').click()
    cy.log("Step 4")
});

And('move on to other purchases' , () => {
   cy.get('.m-controls > .btn--index').click()
    cy.log("Step 5")
});

Then('select the second section with technology' ,() =>{
    cy.contains('[aria-label="4 / 13"] > .nav-item')
        .should('[aria-label="4 / 13"] > .nav-item').click()
    cy.log("Step 6")
});

And('choose the second technique' , () => {
    cy.contains('[aria-label="5 / 28"] > .card-product > .c-description > .c-text').click()
    cy.log("Step 7")
});

Then('add to cart and check' , () => {
    cy.get('.pp-controls > .btn--block').
    should('.pp-controls > .btn--block').click()
    cy.log("Step 8")
});

Then('go to cart' , () => {
   cy.get('.m-controls > [href="/cart"]').click()
    cy.log("Step 9")
});

Then('we place an order' ,() => {
    cy.get('.shopping-order > .btn').click()
    cy.log("Step 10")
});


Then('select the design as a guest' ,() => {
    cy.get('.modal-popup__grid > :nth-child(2) > .btn')
    cy.log("Step 11")
});


Then('we place an order and check it ', () => {
    cy.get('#name').type('Iva Van Ivan')
    cy.get('#email').type('ivanivaov@gmail.com')
    cy.get('#phone').type('29222222')
    cy.get(':nth-child(2) > .inp-ic-autocomplite > .inp').type('mockovckau')
    cy.get('[data-v-7abdfe51=""][data-v-14888d1b=""] > .inp').type('Как писать fluent api')
    cy.get(':nth-child(3) > .form-row > :nth-child(1) > .inp').type('2')
    cy.get('.form-row > :nth-child(3) > .inp').type('2')
    cy.get(':nth-child(4) > .form-row > :nth-child(1) > .inp').type('2')
    cy.get(':nth-child(4) > .form-row > :nth-child(2) > .inp').type('2')
    cy.get('.checkout-section > :nth-child(1) > :nth-child(1) > .inp-box__label > .inp-box__row > .inp-box__text')
    cy.get(':nth-child(1) > .filters-body > .filters-wrap > :nth-child(1) > .inp-box > .inp-box__label > .inp-box__row > .inp-box__text')
    cy.log("Step 12")
});




