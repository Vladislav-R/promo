Feature: Checking the functionality of the site on
    purchase of home appliances by guests

    Scenario: We check the choice of equipment, ordering, purchase
        Given site opens 5element
        And select the first section with technology
        And choose the first technique
        And add it to the cart , check that the product has been added
        And move on to other purchases
        Then select the second section with technology
        And choose the second technique
        Then add to cart and check
        Then go to cart
        Then we place an order
        Then select the design as a guest
        Then we place an order and check it
