import { When, Then, Given } from 'cypress-cucumber-preprocessor/steps';



Given('open website ', () => {
    cy.visit('https://5element.by/catalog/2297-chasy-naruchnye')
    cy.log("Step 1")
});
Then('open login panel', () => {
    cy.contains("Вход").click()
    cy.log("Step 2")
});
Then('login with sms',  () => {
    cy.contains("Войти с помощью смс").click()
    cy.log("Step 3")
});
Then('go password input panel , back to sms, enter data ' ,  () =>  {
    cy.contains("Войти с помощью пароля").click()
    cy.contains("Войти с помощью смс").click()
    cy.get('form.form-section > :nth-child(1) > .inp').type('291111111')
    cy.get("form.form-section > :nth-child(2)").should("form.form-section > :nth-child(2)"). click()
    cy.log("Step 4")
});
